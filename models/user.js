const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
  name: String,
  username: String,
  email: String,
  hash: String,
  salt: String,
  role: { type: String, default: 'CLIENT' },
  cart: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Product'
    }
  ],
  orders: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Order'
    }
  ],
  createdAt: {
    type: Date,
    default: Date.now
  }
})

const userModel = mongoose.model('User', userSchema)

module.exports = userModel
