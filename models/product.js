const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
  name: String,
  price: Number,
  description: String,
  quantity: Number,
  createdAt: {
    type: Date,
    default: Date.now
  }
})

const productModel = mongoose.model('Product', productSchema)

module.exports = productModel
