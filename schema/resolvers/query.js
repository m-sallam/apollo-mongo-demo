const User = require('../../models/user')
const { verify } = require('../../auth')

module.exports = {
  async user (parent, { username }, ctx, info) {
    const user = await User.findOne({ username }).populate('orders products')
    if (!user) return null
    return user
  },
  async users (parent, { username, limit = 0 }, ctx, info) {
    const users = await User.find({ username }).limit(limit).populate('orders products')
    if (!users.length) return null
    return users
  },
  async me (parent, args, { token }, info) {
    if (!token) return null
    const { username } = await verify(token)
    if (!username) return null
    const user = await User.findOne({ username }).populate('orders products')
    if (!user) return null
    return user
  }
}
