const Mutation = require('./mutation')
const Query = require('./query')

const resolver = {
  Mutation,
  Query
}

module.exports = resolver
