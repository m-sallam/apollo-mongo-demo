const User = require('../../models/user')
const Product = require('../../models/product')
const { generateSalt, generateHash, generateToken, verify } = require('../../auth')

module.exports = {
  async user (parent, { username, email, password, name }, ctx, info) {
    try {
      const salt = await generateSalt()
      const hash = await generateHash(password, salt)
      const token = await generateToken({ username })
      const newUser = new User({ name, username, email, hash, salt })
      await newUser.save()
      return {
        success: true,
        message: 'user registered successfuly',
        user: newUser,
        token
      }
    } catch (err) {
      console.log(err)
      return {
        success: false,
        message: err.message
      }
    }
  },

  async product (parent, { name, price, description, quantity }, { token }, info) {
    if (!token) return { success: false, message: 'You need to be logged in', product: null }
    const { username } = await verify(token)
    if (!username) return { success: false, message: 'You need to be logged in', product: null }
    const user = await User.findOne({ username }).populate('orders products')
    if (!user) return null
    if (user.role !== 'ADMIN') return null
    const newProduct = new Product({ name, price, description, quantity })
    await newProduct.save()
    return {
      success: true,
      message: 'product added successfully',
      product: newProduct
    }
  }
}
