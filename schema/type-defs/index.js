const { gql } = require('apollo-server-express')
const userTypes = require('./user')
const productTypes = require('./product')
const orderTypes = require('./order')

const root = gql`
  type Query {
    root: String
  }

  type Mutation {
    root: String
  }
`

module.exports = [root, userTypes, productTypes, orderTypes]
