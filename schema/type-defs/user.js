const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type User {
    id: ID 
    name: String!
    username: String! 
    email: String! 
    cart: [Product]
    orders: [Order]
    role: UserRole
  }

  extend type Query {
    user(username: String!): User
    users(username: String, limit: Int): [User]
    me: User
  }

  extend type Mutation {
    user(username: String!, name: String!, email: String!, password: String!): UserMutationResponse
  }

  type UserMutationResponse {
    success: Boolean
    message: String
    user: User
    token: String
  }

  enum UserRole {
    ADMIN
    CLIENT
  }
`
module.exports = typeDefs
