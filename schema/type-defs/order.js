const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type Order {
    user: User!
    orders: [Order!]!
  }
`
module.exports = typeDefs
