const { gql } = require('apollo-server-express')

const typeDefs = gql`
  type Product {
    name: String!
    price: Int!
    description: String
    quantity: Int!
    user: User!
  }

  extend type Mutation {
    product(name: String!, price: Int!, description: String, quantity: Int!):ProductMutationResponse
  }

  type ProductMutationResponse {
    success: Boolean
    message: String
    product: Product
  }
`
module.exports = typeDefs
