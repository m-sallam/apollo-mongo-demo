const crypto = require('crypto')
const util = require('util')
const jwt = require('jsonwebtoken')

// generating a salt with a length of 32 bytes to be used in the paswword hashing
const generateSalt = async () => {
  const randomBytes = util.promisify(crypto.randomBytes)
  const salt = await randomBytes(32)
  return salt.toString('base64')
}

// hashing the given password with Password-Based Key Derivation Function 2
const generateHash = async (password, salt) => {
  const getHash = util.promisify(crypto.pbkdf2)
  const hash = await getHash(password, salt, 2000000, 64, 'sha512')
  return hash.toString('base64')
}

const generateToken = async payload => {
  const token = jwt.sign(payload, process.env.APP_SECRET, { expiresIn: '7d' })
  const bearerToken = 'Bearer ' + token
  return bearerToken
}

// hashing the entered password with the user's salt, and then comparing the 2 hashes.
// if it is successful, a json web token is created and returned
const authenticate = async (payload, password, hashedPassword, salt) => {
  const hashedEntry = await generateHash(password, salt)
  const matched = crypto.timingSafeEqual(Buffer.from(hashedEntry), Buffer.from(hashedPassword))
  if (!matched) return false
  const token = await generateToken(payload)
  return token
}

// verifying user using the authorization header
const verify = async header => {
  try {
    const token = header.split('Bearer ')[1]
    const payload = jwt.verify(token, process.env.APP_SECRET)
    return payload
  } catch (err) {
    console.log(err)
    return false
  }
}

// refreshing the request token with a new token with a new expiration date
const refreshToken = async oldToken => {
  try {
    const payload = jwt.verify(oldToken, process.env.APP_SECRET)
    delete payload.iat
    delete payload.exp
    delete payload.nbf
    delete payload.jti
    const token = jwt.sign(payload, process.env.APP_SECRET, { expiresIn: '7d' })
    const bearerToken = 'Bearer ' + token
    return bearerToken
  } catch (err) {
    console.log(err)
    return false
  }
}

module.exports = { generateSalt, generateHash, authenticate, generateToken, verify, refreshToken }
