require('dotenv').config()

const express = require('express')
const { ApolloServer } = require('apollo-server-express')
const mongoose = require('mongoose')

const typeDefs = require('./schema/type-defs')
const resolvers = require('./schema/resolvers')

require('./models/order')
require('./models/product')

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({ token: req.headers.authorization || null })
})

const app = express()
server.applyMiddleware({ app, path: '/graphql' })

const bootstrap = async () => {
  await mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  await app.listen({ port: 3000 })
  console.log('Server ready at http://localhost:3000/graphql')
}

bootstrap()
